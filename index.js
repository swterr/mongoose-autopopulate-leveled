var mongoose = require('mongoose');
var depth = 1, populateLevel;

module.exports = function(schema) {
  schema.statics.populateLevel = 1;
  schema.statics.setPopulateLevel =
  function setPopulateLevel(level) {
	schema.statics.populateLevel = level;
  };
  

  var pathsToPopulate = [];

  eachPathRecursive(schema, function(pathname, schemaType) {
    var option;
    if (schemaType.options && schemaType.options.autopopulate) {
      option = schemaType.options.autopopulate;
      pathsToPopulate.push({
        options: defaultOptions(pathname, schemaType.options),
        autopopulate: option
      });
    } else if (schemaType.options &&
        schemaType.options.type &&
        schemaType.options.type[0] &&
        schemaType.options.type[0].autopopulate) {
      option = schemaType.options.type[0].autopopulate;
      pathsToPopulate.push({
        options: defaultOptions(pathname, schemaType.options.type[0]),
        autopopulate: option
      });
    }
  });

  if (schema.virtuals) {
    Object.keys(schema.virtuals).forEach(function(pathname) {
      if (schema.virtuals[pathname].options.autopopulate) {
        pathsToPopulate.push({
          options: defaultOptions(pathname, schema.virtuals[pathname].options),
          autopopulate: schema.virtuals[pathname].options.autopopulate,
        });
      }
    });
  }

  var autopopulateHandler = function() {
    if(!populateLevel ||
      (populateLevel && schema.statics.populateLevel && schema.statics.populateLevel != populateLevel)) {
      var firsttime = true;
      populateLevel = schema.statics.populateLevel;
      depth = 1;
    }

    depth++;

	if (this._mongooseOptions && this._mongooseOptions.lean) return;
    var numPaths = pathsToPopulate.length;
    for (var i = 0; i < numPaths; ++i) {
	    console.log('populateLevel set to', populateLevel, 'and depth now is', depth, 'so we',
        ((depth<=populateLevel)? 'populate': 'do not populate'), pathsToPopulate[i].options.path)

        if(depth<=populateLevel)
          processOption.call(this,
              pathsToPopulate[i].autopopulate,
              pathsToPopulate[i].options);
    }

    if(firsttime)
      firsttime = false;
    else depth--;
  };

  schema.
    pre('find', autopopulateHandler).
    pre('findOne', autopopulateHandler);
};

function defaultOptions(pathname, v) {
  var ret = { path: pathname };
  if (v.ref) {
    ret.model = v.ref;
  }
  return ret;
}

function processOption(value, options, cb) {
  switch (typeof value) {
    case 'function':
      handleFunction.call(this, value, options, cb);
      break;
    case 'object':
      handleObject.call(this, value, options, cb);
      break;
    default:
      handlePrimitive.call(this, value, options, cb);
      break;
  }
}

function handlePrimitive(value, options, cb) {
  if (value) {
    this.populate(options);
  }
}

function handleObject(value, optionsToUse, cb) {
  mergeOptions(optionsToUse, value);
  this.populate(optionsToUse);
}

function handleFunction(fn, options) {
  var val = fn.call(this);
  processOption.call(this, val, options);
}

function mergeOptions(destination, source) {
  var keys = Object.keys(source);
  var numKeys = keys.length;
  for (var i = 0; i < numKeys; ++i) {
    destination[keys[i]] = source[keys[i]];
  }
}

function eachPathRecursive(schema, handler, path) {
  if (!path) {
    path = [];
  }
  schema.eachPath(function(pathname, schemaType) {
    path.push(pathname);
    if (schemaType.schema) {console.info('!!!!!!! Recursion');
      eachPathRecursive(schemaType.schema, handler, path);
    } else {
      handler(path.join('.'), schemaType);
    }

    path.pop();
  });
}
